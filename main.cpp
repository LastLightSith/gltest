/*
 * Created on 8-2-2019
 * By smit
 * <smit17av@gmail.com>
 */


#ifndef NDEBUG
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#endif

#ifdef NDEBUG
#include <glad/glad.h>
#endif

#include "stb_image.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <map>

static const char* fShaderSrc =
        #include <fragment.hstr>
        ;
static const char* vShaderSrc =
        #include <vertex.hstr>
        ;

void shaderCompileError(GLuint shader,const char*name) {
    int  success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(shader, 512, nullptr, infoLog);
        std::cout << "ERROR::SHADER::" << name << "::COMPILATION_FAILED\n" << infoLog << std::endl;
    }
}

static const int winWIDTH  = 1200;
static const int winHIEGHT = 800;

int main(/*int argc, char *argv[]*/) {

    SDL_Init(SDL_INIT_VIDEO);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,3);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE,8);

    glEnable(GL_MULTISAMPLE);


    int center = SDL_WINDOWPOS_CENTERED;
    auto win = SDL_CreateWindow("GLTest",center,center,winWIDTH,winHIEGHT, SDL_WINDOW_OPENGL);
    auto winGLcon = SDL_GL_CreateContext(win);

    glViewport(0,0,winWIDTH,winHIEGHT);

    glEnable(GL_DEPTH_TEST);

#ifdef NDEBUG
    if(!gladLoadGL()) {
        throw std::runtime_error("Error while loading glad");
    }

#endif
//    float vertices[] = {
//        -0.5f, -0.5f, 0.0,  //bottom-leff
//         0.5f, -0.5f, 0.0,  //bottom-right
//         0.0f,  0.5f, 0.0,   //top
//         0.0f,  0.5f, 0.0,   //top
//        -0.5f, -0.5f, -0.5,  //bottom-leff
//         0.5f, -0.5f, -0.5  //bottom-right
//    };

    float vertices[] = {
        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,

        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f, -0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,

         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,

        -0.5f, -0.5f, -0.5f,
         0.5f, -0.5f, -0.5f,
         0.5f, -0.5f,  0.5f,
         0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f,  0.5f,
        -0.5f, -0.5f, -0.5f,

        -0.5f,  0.5f, -0.5f,
         0.5f,  0.5f, -0.5f,
         0.5f,  0.5f,  0.5f,
         0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f,  0.5f,
        -0.5f,  0.5f, -0.5f,
    };

    float colors[] ={
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
        1.0f,0.0f,0.0f, // bottom left
        0.0f,1.0f,0.0f, // bottom right
        0.0f,0.0f,1.0f,  // top
        0.0f,0.0f,1.0f,  // top
        0.0f,1.0f,0.0f, // bottom right
        1.0f,0.0f,0.0f, // bottom left
    };

    float texCoords[] = {
//       -2.0f, 0.0f,  // lower-left corner
//        2.0f, 0.0f,  // lower-right corner

//        0.5f, 2.0f ,  // top-center corner
//        0.5f, 2.0f ,  // top-center corner

//       -2.0f, 0.0f,  // lower-left corner
//        2.0f, 0.0f,  // lower-right corner

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,

        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,

        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f,

        0.0f, 1.0f,
        1.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 0.0f,
        0.0f, 0.0f,
        0.0f, 1.0f

    };

    GLuint vao=0;
    glGenVertexArrays(1,&vao);
    glBindVertexArray(vao);

    GLuint vbo[3];
    glGenBuffers(3,vbo);

    //buffer creataion

    glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
    glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
    glBufferData(GL_ARRAY_BUFFER,sizeof(colors),colors,GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER,vbo[2]);
    glBufferData(GL_ARRAY_BUFFER,sizeof(texCoords),texCoords,GL_STATIC_DRAW);


    //buffer bing with vertex attrib pointer

    glBindBuffer(GL_ARRAY_BUFFER,vbo[0]);
    glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,nullptr);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER,vbo[1]);
    glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,sizeof(float)*3,nullptr);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER,vbo[2]);
    glVertexAttribPointer(2,2,GL_FLOAT,GL_FALSE,sizeof(float)*2,nullptr);
    glEnableVertexAttribArray(2);

    //shaders
    GLuint vShader = glCreateShader(GL_VERTEX_SHADER),
           fShader =  glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vShader,1,&vShaderSrc,nullptr);
    glShaderSource(fShader,1,&fShaderSrc,nullptr);

    glCompileShader(vShader);
    shaderCompileError(vShader,"vertex");
    glCompileShader(fShader);
    shaderCompileError(fShader,"fragment");

    GLuint program = glCreateProgram();
    glAttachShader(program,vShader);
    glAttachShader(program,fShader);
    glLinkProgram(program);

    glUseProgram(program);

    //texture

    int height,width,n_channel;
    stbi_set_flip_vertically_on_load(1);
    unsigned char *texData;

    GLuint tex[2];
    glGenTextures(2,tex);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D,tex[0]);
    texData= stbi_load("aangT.png",&width,&height,&n_channel,3);
    if(!texData) throw std::runtime_error("Cant load image");
    glUniform1i(glGetUniformLocation(program,"tex1"),0);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,texData);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(texData);

    //texture 2

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D,tex[1]);
    texData = stbi_load("wall.jpg",&width,&height,&n_channel,3);
    if(!texData) throw std::runtime_error("Cant load image");
    glUniform1i(glGetUniformLocation(program,"tex2"),1);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_MIRRORED_REPEAT);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,width,height,0,GL_RGB,GL_UNSIGNED_BYTE,texData);
    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(texData);

    //tranformation


    //main loop
    SDL_Event event;
    float delta=0;
    const float speed = 1;
    float x=0,y=0,z=0;

    int mx=0,my=0;bool moveWithMouse=false;
    float lastX = 400, lastY = 300;

    std::map<char,bool> key;
    key['a'] = false;
    key['b'] = false;
    key['c'] = false;
    key['d'] = false;
    key['z'] = false;
    key['S'] = false;


    glm::mat4 tranform(1),
              view(1),
              model(1),
              projection(1);

    auto cameraPos   = glm::vec3(0.0f, 0.0f, 3.0f);
    auto cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
    auto cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);

    float yaw   = -90.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
    float pitch =  0.0f;
    bool firstM=false;

    view = glm::translate(view, glm::vec3(0.0f, 0.0f, -2.0));
    model = glm::rotate(model,glm::radians(0.0f),glm::vec3(1,0,0));
    glUniformMatrix4fv(glGetUniformLocation(program,"model"),1,false,glm::value_ptr(model));

    projection = glm::perspective(glm::radians(45.0f), (float)winWIDTH/ winHIEGHT,0.1f, 100.0f);
//    projection = glm::ortho(0.0f, 800.0f, 0.0f, 600.0f, 0.1f, 100.0f);
    glUniformMatrix4fv(glGetUniformLocation(program,"projection"),1,false,glm::value_ptr(projection));

    int rotaion=6;bool rPause=false;
    for(bool running=true;running;){
        float framInitTime = SDL_GetTicks()/1000.0f;

        while(SDL_PollEvent(&event) ) {
            if(event.type == SDL_QUIT) {
                running = false;
                break;
            }
            if(event.key.keysym.mod & KMOD_SHIFT)
                key['S'] = true;

            if(event.key.state == SDL_PRESSED) {
                switch (event.key.keysym.sym) {
                case SDLK_a:
                    key['a'] = true;
                    break;
                case SDLK_d:
                    key['d'] = true;
                    break;
                case SDLK_w:
                    key['w'] = true;
                    break;
                case SDLK_s:
                    key['s'] = true;
                    break;

                case SDLK_SPACE:
                    rotaion+=100;
                    break;
                case SDLK_p:
                    rPause = !rPause;
                    break;
                case SDLK_z:
                    moveWithMouse = true;
                    SDL_CaptureMouse(SDL_TRUE);
                    break;
                }
            }
            else if(event.key.state == SDL_RELEASED){
                switch (event.key.keysym.sym){
                case SDLK_a:
                    key['a'] = false;
                    break;
                case SDLK_d:
                    key['d'] = false;
                    break;
                case SDLK_s:
                    key['s'] = false;
                    break;
                case SDLK_w:
                    key['w'] = false;
                    break;

                case SDLK_z:
                    moveWithMouse = false;
                    break;
                }
            }


            if(moveWithMouse) {
                if(firstM) {
                    firstM=false;
                    lastX = winWIDTH/2;
                    lastY = winHIEGHT/2;
                }


                if(event.type == SDL_MOUSEMOTION){
                    mx = event.motion.x;
                    my = event.motion.y;
                }
                float xoffset = mx- lastX;
                float yoffset = lastY - my;
                lastX = mx;
                lastY = my;
                std::cerr << "x:" << mx << " y:" << my << '\n';

                float sensitivity = 0.1;
                xoffset *= sensitivity;
                yoffset *= sensitivity;

                yaw   += xoffset;
                pitch += yoffset;

                if(pitch > 89.0f)
                    pitch = 89.0f;
                if(pitch < -89.0f)
                    pitch = -89.0f;

                glm::vec3 front;
                front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
                front.y = sin(glm::radians(pitch));
                front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
                cameraFront = glm::normalize(front);
            }
        }

        auto Speed = speed*delta;
        if(key['a']){
            cameraPos += Speed *glm::normalize(glm::cross(cameraFront, cameraUp)) ;
        }
        if(key['d']){
            cameraPos -= Speed *glm::normalize(glm::cross(cameraFront, cameraUp)) ;
        }
        if(key['w']) {
            cameraPos -= Speed *glm::vec3(0,1,0) ;
        }
        if(key['s']) {
            cameraPos += Speed *glm::vec3(0,1,0) ;
        }

        if(rotaion >= 45) rotaion-=5;
        if(rPause) tranform = glm::rotate(tranform,glm::radians(rotaion*delta),glm::vec3(1,1,1));

        view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

        glUniformMatrix4fv(glGetUniformLocation(program,"tranform"),1,false,glm::value_ptr(tranform));

        glUniformMatrix4fv(glGetUniformLocation(program,"view"),1,false,glm::value_ptr(view));

        glDrawArrays(GL_TRIANGLES,0,36);

        SDL_GL_SwapWindow(win);
        glClearColor(0,0,0,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        delta = SDL_GetTicks()/1000.0f - framInitTime;
    }

    SDL_DestroyWindow(win);
    SDL_GL_DeleteContext(winGLcon);

    return 0;
}
