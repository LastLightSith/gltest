#version 330 core

layout (location = 0) in vec3 apos;
layout (location = 1) in vec3 _mycolor;
layout (location = 2) in vec2 _mytex;

out vec3 mycolor;
out vec2 mytex;

uniform mat4 tranform;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void)
{
    mycolor = _mycolor;
    mytex = _mytex;

    gl_Position = projection * view * model * tranform * vec4(apos,1);
}
