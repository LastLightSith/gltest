#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

for i in sys.argv[2:]:
    src = open(i,"r")
    tmp = "R\"glsl(" + src.read() + ")glsl\""
    outPath = sys.argv[1] + "/" + i.split(".")[0].split('/')[-1] + ".hstr" #path to file in sys.argv[1] dir with .hstr extention
    out = open(outPath,"w")
    out.write(tmp)
