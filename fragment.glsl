#version 330 core
out vec4 FragColor;

in vec3 mycolor;
in vec2 mytex;

uniform sampler2D tex1;
uniform sampler2D tex2;


void main(void)
{
        vec4 color = vec4(mycolor,1.0f);
        color *= mix(texture2D(tex2, mytex), texture2D(tex1, mytex), 0.5);
        FragColor = color;
}
